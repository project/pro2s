<?php

function _drupalsquare_resource_access()
{
    return true;
}



 function cleanKeys($arr)
 {
     $keys = array_keys($arr);

     foreach ($keys as $key) {
         $newKey = str_replace('field_idv_', '', $key);
         $arr[$newKey] = $arr[$key];
         unset($arr[$key]);
     }
     return $arr;
 }


function camelKeys($arr)
{
    $keys = array_keys($arr);

    foreach ($keys as $key) {
        $newKey = str_replace('_', ' ', $key);
        $value = $arr[$key];
        unset($arr[$key]);
        $arr[$newKey] = $value;
    }
    return $arr;
}

function mapKeys($arr, $func)
{
    $keys = array_keys($arr);

    foreach ($keys as $key) {
        $newKey = $func($key);
        $value = $arr[$key];
        unset($arr[$key]);
        $arr[$newKey] = $value;
    }
    return $arr;
}

function pipeKey($arr, $func)
{
    $realArgs = array_splice(func_get_args(), 1);
    foreach ($realArgs as $arg) {
        $arr = mapKeys($arr, $arg);
    }
    return $arr;
}

function pipeValue($arr, $func)
{
    $realArgs = array_splice(func_get_args(), 1);
    foreach ($realArgs as $arg) {
        $arr = array_map($arg, $arr);
    }
    return $arr;
}

function map($arr, $funcKey)
{
}


function camelCase($str, array $noStrip = [])
{
    // non-alpha and non-numeric characters become spaces
    $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
    $str = trim($str);
    // uppercase the first character of each word
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    $str = lcfirst($str);

    return $str;
}

function clean($elem)
{
    if ((gettype($elem) == 'array')) {
        if (count($elem) == 0) {
            return null;
        } elseif (array_key_exists('und', $elem)) {
            if (count($elem['und'][0] == 1 || count($elem['und'][0]) == 3) && array_key_exists('value', $elem['und'][0])) {
                return $elem['und'][0]['value'];
            } else {
                return $elem['und'][0];
            }
        }
    } else {
        return $elem;
    }
}

$removeUnd = function ($elem) {
    if (gettype($elem) == 'array') {
        return '$elem';
    } else {
        return $elem;
    }
};

function newClean($user)
{
    return pipeValue($user, $removeUnd, $simplify, $checkForEmptyArray);
};

  function removeField($elem)
  {
      return str_replace('field', '', $elem);
  }


function _drupalsquare_resource_retrieve($uid)
{
    $account = user_load($uid);
    //$profile2 = profile2_load($uid)

    // $types = profile2_get_types();
    //   if (!empty($types)) {
    //       foreach ($types as $type) {
    //           $profile = profile2_load_by_user($uid, $type->type);
    //       }
    //   }



    //$profile["individual"]->field_idv_name_middle['und'][0]['value'] = 'matty';
    //profile2_load($uid);
    //$profile.save();
    //profile2_save($profile["individual"]);
    //$profile->save();
    //$pro = new Profile();
    //$profileNew = profile2_load_by_user($uid);
    // $pro.$pid = 666;
    //$pro.buildContent();
    //pro->save();




    $removeUnderscore = function ($arg) {
        return str_replace('_', ' ', $arg);
    };
    function toUpper($arg)
    {
        return strtoupper($arg);
    }

    $upper = function ($arg) {
        return strtoupper($arg);
    };

    $removeSpace = function ($arg) {
        return str_replace(' ', '', $arg);
    };

    $uFirst = function ($arg) {
        return ucfirst($arg);
    };
    $cleanKey = function ($arg) {
        return str_replace('field_idv_', '', $arg);
    };


    $removeUnd = function ($elem) {
        if (gettype($elem) == 'array') {
            if (array_key_exists('und', $elem)) {
                return $elem['und'][0];
            } else {
                return $elem;
            }
        } else {
            return $elem;
        }
    };

    $simplify = function ($elem) {
        if (gettype($elem) == 'array') {
            if (array_key_exists('value', $elem)) {
                return $elem['value'];
            } else {
                return $elem;
            }
        } else {
            return $elem;
        }
    };

    $checkForEmptyArray = function ($elem) {
        if (gettype($elem) == 'array' && count($elem) == 0) {
            return null;
        } else {
            return $elem;
        }
    };

    $cleanProfile = function ($profile) use ($removeUnd, $simplify, $checkForEmptyArray, $cleanKey) {
        $profile = get_object_vars($profile);
        $profile = pipeValue($profile, $removeUnd, $simplify, $checkForEmptyArray);
        $profile = pipeKey($profile, $cleanKey, removeField, camelCase);
        return $profile;
    };

    $account = user_load($uid);
    $profile = profile2_load_by_user($uid);
    $individual = profile2_load_by_user($uid, 'individual');
    $investor = profile2_load_by_user($uid, 'investor');

    $individual = $cleanProfile($individual);
    $investor = $cleanProfile($investor);

    return array(
    '$individual' => $individual,
    '$investor' => $investor,
    'individual' => $individual,
  );

    // if (isset($account->data['checked_in'])) {
  //   return array(
  //     'uid' => $account->uid,
  //     'name' => $account->name,
  //     'last_checkin' => $account->data['checked_in'],
  //   );
  // }
  // else {
  //   return FALSE;
  // }
}

function _drupalsquare_resource_checkin($uid, $date = null)
{
    $account = user_load($uid);
    $account->data['checked_in'] = is_null($date) ? REQUEST_TIME : $date;
    user_save($account);
    return true;
}
